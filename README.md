# master-thesis-protorepo
To compile the *Protobuf*-files, following packages are needed:
* [Compiler](https://github.com/protocolbuffers/protobuf)
* [gRPC Java-Plugin](https://github.com/grpc/grpc-java/blob/master/compiler/README.md) - Build via Gradle and reference via compiler argument.
* [gRPC JS-Plugin](https://github.com/grpc/grpc-web) - Install via NPM.
* [Rust Code Generator](https://crates.io/crates/protobuf-codegen) and [gRPC Rust-Plugin](https://crates.io/crates/grpcio-compiler) - Install via Cargo and reference via compiler argument.

When the compiler has been installed successfully, *.proto*-files with *gRpc*-stubs can be compiled with:
```
# Java:
protoc -I=${PROTO_SRC}  ${PROTO_SRC}/*.proto \
    --java_out=${PROTO_TARGET} \
    --plugin=protoc-gen-grpc-java=${GRPC_JAVA_PLUGIN} \
    --grpc-java_out=${GRPC_TARGET}

# JS:
protoc -I=${PROTO_SRC} ${PROTO_SRC}/*.proto \
    --js_out=import_style=commonjs:${PROTO_TARGET} \
    -grpc-web_out=import_style=commonjs,mode=grpcwebtext:${GRPC_TARGET}

# Rust
protoc -I=${PROTO_SCR} ${PROTO_SCR}/*.proto \
    --rust_out=${PROTO_TARGET} \
    --plugin=protoc-gen-rust-grpc=${which protoc-gen-rust-grpc} \
    --grpc-rust_out=${GRPC_TARGET}
    
protoc -I=${PROTO_SCR} ${PROTO_SCR}/dago-backend/*.proto \
    --rust_out=${PROTO_TARGET} \
    --plugin=protoc-gen-grpc=${which protoc-gen-rust-grpc} \
    --grpc_out=${GRPC_TARGET}
```